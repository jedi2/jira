import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

import './App.css';

const columnsInitial = [
  {
    name: 'todo',
    color: 'secondary',
    tasks: [
      {
        id: 1,
        name: 'todo 1'
      },
      {
        id: 2,
        name: 'todo 2'
      },
      {
        id: 3,
        name: 'todo 3'
      },
    ]
  },
  {
    name: 'progress',
    color: 'primary',
    tasks: [
      {
        id: 4,
        name: 'todo 4'
      },
      {
        id: 5,
        name: 'todo 5'
      },
      {
        id: 6,
        name: 'todo 6'
      },
    ]
  },
  {
    name: 'review',
    color: 'warning',
    tasks: [
      {
        id: 7,
        name: 'todo 7'
      },
      {
        id: 8,
        name: 'todo 8'
      },
      {
        id: 9,
        name: 'todo 9'
      },
    ]
  },
  {
    name: 'done',
    color: 'success',
    tasks: [
      {
        id: 10,
        name: 'todo 10'
      },
      {
        id: 12,
        name: 'todo 11'
      },

    ]
  },
];
function App() {
  const [columns, setColumns] = useState(columnsInitial)

  const rerangeTasks = (arg) => {
    const reranged = columns.map(col => {
      if (col.name === arg.columnName) {

        const tasks = arg.direction === 'up'
          ? swapUp(col.tasks, arg.taskId)
          : swapDown(col.tasks, arg.taskId);

        return { ...col, tasks }
      } else return col
    })

    setColumns(reranged)
  };

  const rerangeHorizontalTasks = (arg) => {
    const colIndex = columns.findIndex(col => col.name === arg.columnName);

    if (arg.direction === 'left' && colIndex <= 0) return;
    if ((arg.direction === 'right' && colIndex === columns.length - 1) || colIndex < 0) return;

    const reRanged = [...columns];

    const task = reRanged[colIndex].tasks.find(el => el.id === arg.taskId);
    
    reRanged[colIndex].tasks = reRanged[colIndex].tasks.filter(el => el.id !== arg.taskId);
    
    if (arg.direction === 'left') {
      reRanged[colIndex - 1].tasks.push(task)
    };

    if (arg.direction === 'right') {
      reRanged[colIndex + 1].tasks.push(task)
    }
    setColumns(reRanged)
  };

  return (
    <div className="container">
      <h1 className='mb-4  mt-4'>Board</h1>
      <div className="container">
        <div className="row">
          {
            columns.map(col => (<div className="col-sm d-flex" key={col.name}>
              <div className={`w-100 border-top border-${col.color} border-width-4`}>
                <h3>{col.name}</h3>
                {
                  col.tasks.map(task => (
                    <div className={`card text-white mb-2 bg-${col.color}`} key={task.id}>
                      <div className="card-body">
                        <h5 className="card-title">{task.name}</h5>
                        <button type="button" className="btn btn-light mt-3 mb-2" onClick={() => rerangeTasks({
                          columnName: col.name,
                          taskId: task.id,
                          direction: 'up'
                        })}><span aria-hidden="true" role="img">⬆️</span></button>
                        <button type="button" className="btn btn-light mt-3 mb-2" onClick={() => rerangeTasks({
                          columnName: col.name,
                          taskId: task.id,
                          direction: 'down'
                        })}><span aria-hidden="true" role="img">⬇️</span></button>

                        <button type="button" className="btn btn-light mt-3 mb-2" onClick={() => rerangeHorizontalTasks({
                          columnName: col.name,
                          taskId: task.id,
                          direction: 'left'
                        })}><span aria-hidden="true" role="img">⬅️</span></button>
                        <button type="button" className="btn btn-light mt-3 mb-2" onClick={() => rerangeHorizontalTasks({
                          columnName: col.name,
                          taskId: task.id,
                          direction: 'right'
                        })}><span aria-hidden="true" role="img">➡️</span></button>






                      </div>
                    </div>
                  ))
                }
              </div>
            </div>))
          }
        </div>
      </div>
    </div>
  );
}

export default App;
function swapUp(arr_, id) {
  const arr = [...arr_];
  let si = arr.findIndex(el => el.id === id);
  if (si <= 0) return arr;

  const prev = arr[si - 1];
  const curr = arr[si];

  arr[si] = prev;
  arr[si - 1] = curr;
  return arr
};
function swapDown(arr_, id) {
  const arr = [...arr_];
  let si = arr.findIndex(el => el.id === id);
  if (si < 0 || si === arr.length - 1) return arr;

  const next = arr[si + 1];
  const curr = arr[si];

  arr[si] = next;
  arr[si + 1] = curr;
  return arr;
}